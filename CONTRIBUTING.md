# **Wie kann ich mich mitmachen?**
**Wir wollen zusammen Welten bauen doch hierfür brauchen wir Pläne, Ideen und Hände. Als Kreativschaffende arbeiten wir im Verbund und bieten Firmen und Industrie neue Perspektiken und innovative Ideen.**

Schreib uns gerne eine Nachricht oder komm zu unserem wöchentlichen Treffen. (Jeden Donnerstag ab 16 Uhr in der Game Lounge des eHaus der HBK )

## **Wer bei uns mitmacht**

Schön das du hier bist, wir freuen uns immmer über neue Gesichter und helfende Hände oder einfach nur interessierte Zuhörer.

Damit die Zusammenarbeit haben wir einen [Verhaltenskodex](https://bigassmessage.com/3bd7c) erstellt, am besten schaust du ihn dir an bevor du tiefer einsteigst. Um genauer zu erfahren was wir machen kann du unser [READ ME](https://gitlab.com/s.co-op/s.co-op/blob/master/README.md) lesen.

Mit folgende Fähigkeiten freuen wir uns ganz besonders über euch:

+ Konzeptentwicklung 
+ Produkt-und Industriedesign
+ Grafikdesign
+ Videobearbeitung
+ Social Media Marketing
+ Webdesign
+ Photografie und Bildbearbeitung
+ Buchhaltung
+ Projektmanagement
+ Aquise
+ Verkauf
+ Handwerkliche Fähigkeiten
+ Holzbearbeitung
+ Metallbearbeitung
+ Programmierung
+ ... (Verdammt viele andere)


Weil wir uns gerne mit regionalen und überregionalen Betrieben, Firmen, Freelancern und Studenten zusammenarbeiten wollen, brauchen wir ein gutes Netzwerk also; erzähl gerne allen die du triffst was wir vorhaben damit wir gemeinsam neue Welten erschaffen können.


## **Wer mit uns arbeiten kann**

+ Kreativschaffende
+ Studenten
+ Vertreter aus Wirtschaft & Politik
+ Influencer
+ Firmen die auf der Suche nach innovativen Ideen, Produkten und Grafiken sind
+ Leute die gemeinsam Ideen umsetzten wollen
+ Politiker
+ Wissenschaftler, 
+ Land, Bund und Stadt



## **Du hast wenig Zeit aber würdest uns gerne finaziell unterstützen**

Wenn du uns finanziel unterstützen willst kannst du mal auf den CrowdFunding Pages, unserer Projekten oder unserem Online-Shop vorbeischauen. 

Damit S.co-op gut wird brauchen wir ganz verschiedene Leute mit ganz verschiedenen Talenten. 

## **Du hast Ideen wie Wir unsere Vision noch besser umsetzen können?**

Schreib uns gerne eine Nachricht oder komm zu unserem wöchentlichen Treffen. (Jeden Donnerstags um 16:00 an der HBKsaar)